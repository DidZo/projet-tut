const Action = {

    DELETE: 0b01,
	UPDATE: 0b10
};

const CityObject = {
	CAMERA: 0b000,
    NODE: 0b001,

	CAR: 0b011,
	PEDESTRIAN: 0b100,

	CAR_WITH_PATH: 0b110
};

const bitLenght = {
	ID: 16,
	POS: 32,
	ANGLE: 16,


	VITESSE: 8,
	VARIANTE: 8,
	ZOOM: 16
};

function decompress(data) {
	var decompressed = "";
	for (var i = 0; i < data.length; i++) {
		decompressed = numToBin(data.charCodeAt(i) - 63, 6) + decompressed;
	}
	return decompressed;
}

function compress(data) {
	var compressed = "";
	for (var j = data.length - 6; j > -6; j -= 6) {
		if (j >= 0)
			compressed += String.fromCharCode(
				parseInt(data.substr(j, 6), 2) + 63
			);
		else
			compressed += String.fromCharCode(
				parseInt(data.substr(0, 6 + j), 2) + 63
			);
	}
	return compressed;
}

function encode(objet) {
	if (objet.action == null) return;

    if (objet.action === Action.DELETE)
		return compress(
			numToBin(objet.id_obj, bitLenght.ID) + numToBin(Action.DELETE, 2)
		);

	let str = "";

	if (objet.type === CityObject.CAR_WITH_PATH) {
		str += numToBin(objet.id_start, bitLenght.ID);
		str += numToBin(objet.id_finish, bitLenght.ID);

		str += numToBin(objet.type, 3);
		str += numToBin(objet.action, 2);

		return compress(str);
	}

	if (objet.type != CityObject.CAMERA)
		str += numToBin(objet.id_obj, bitLenght.ID);

	if (
		objet.type === CityObject.CAR ||
		objet.type === CityObject.PEDESTRIAN
	)
		str += numToBin(objet.variante, bitLenght.VARIANTE);

	str += numToBin(encodeFloat(objet.posx), bitLenght.POS);
	str += numToBin(encodeFloat(objet.posy), bitLenght.POS);

	if (objet.type == CityObject.CAMERA)
		str += numToBin(objet.zoom, bitLenght.ZOOM);
	else {
		str += numToBin(objet.angle, bitLenght.ANGLE);

		if (
			objet.type === CityObject.PEDESTRIAN ||
			objet.type === CityObject.CAR
		)
			str += numToBin(objet.vitesse, bitLenght.VITESSE);
	}

	str += numToBin(objet.type, 3);
	str += numToBin(objet.action, 2);

	return compress(str);
}

function decode(chunk) {
	let data = decompress(chunk);

	let action = parseInt(data.substr(data.length - 2, 2), 2);
	let cityObjet = parseInt(data.substr(data.length - 5, 3), 2);

	if (action === Action.DELETE)
		return {
			action: action,
			id_obj: parseInt(data.substr(0, data.length - 2), 2)
		};

	let offset = data.length - 5;

    let vitesse = 0;
    let angle = 0;
	if (cityObjet === CityObject.PEDESTRIAN || cityObjet === CityObject.CAR) {
		offset -= bitLenght.VITESSE;
        vitesse = parseInt(data.substr(offset, bitLenght.VITESSE), 2);
        
        offset -= bitLenght.ANGLE;
	    angle = parseInt(data.substr(offset, bitLenght.ANGLE), 2);
	}

	

	offset -= bitLenght.POS;
	let posy = decodeFloat(data.substr(offset, bitLenght.POS));

	offset -= bitLenght.POS;
	let posx = decodeFloat(data.substr(offset, bitLenght.POS));

	let variante = 0;
	if (
		cityObjet === CityObject.CAR ||
		cityObjet === CityObject.PEDESTRIAN
	) {
		offset -= bitLenght.VARIANTE;
		variante = parseInt(data.substr(offset, bitLenght.VARIANTE), 2);
	}

    offset -= bitLenght.ID;
    let offoffset = 0;
    if(offset < 0) {
        offoffset = offset;
        offset = 0;
    }
	let id_obj = parseInt(data.substr(offset, bitLenght.ID + offoffset), 2);
	
	offset -= 1;
	let user = data.substr(offset, 1);
	let userbool = false;
	if(user == "1")
		userbool = true;

	return {
		action: action,
		type: cityObjet,
		id_obj: id_obj,
		variante: variante,
		posx: posx,
		posy: posy,
		angle: angle,
		vitesse: vitesse,
		user : userbool
	};
}

// Debuggage = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

/*
const obj = {
    action: Action.UPDATE,
    type: CityObject.CAR,
    id_obj: 45,

    variante: 8,
    posx: 889.3,
    posy: 182.004,
    angle: 13,
    vitesse: 2,
    zoom : 2,

    id_start: 44,
    id_finish: 22
};

var coded = encode(obj);

console.log(coded);
console.log(toUnicode(coded));
console.log(decode(coded));*/
