// Based on code from Jonas Raoni Soares Silva
// http://jsfromhell.com/classes/binary-parser
// Transforme un nombre réel en un entier avec la meme représentation binaire
function encodeFloat(number) {
	var n = +number,
		status = n !== n || n == -Infinity || n == +Infinity ? n : 0,
		exp = 0,
		len = 281, // 2 * 127 + 1 + 23 + 3,
		bin = new Array(len),
		signal = (n = status !== 0 ? 0 : n) < 0,
		n = Math.abs(n),
		intPart = Math.floor(n),
		floatPart = n - intPart,
		i,
		lastBit,
		rounded,
		j,
		exponent;

	if (status !== 0) {
		if (n !== n) {
			return 0x7fc00000;
		}
		if (n === Infinity) {
			return 0x7f800000;
		}
		if (n === -Infinity) {
			return 0xff800000;
		}
	}

	i = len;
	while (i) {
		bin[--i] = 0;
	}

	i = 129;
	while (intPart && i) {
		bin[--i] = intPart % 2;
		intPart = Math.floor(intPart / 2);
	}

	i = 128;
	while (floatPart > 0 && i) {
		(bin[++i] = ((floatPart *= 2) >= 1) - 0) && --floatPart;
	}

	i = -1;
	while (++i < len && !bin[i]);

	if (
		bin[
			(lastBit =
				22 +
				(i =
					(exp = 128 - i) >= -126 && exp <= 127
						? i + 1
						: 128 - (exp = -127))) + 1
		]
	) {
		if (!(rounded = bin[lastBit])) {
			j = lastBit + 2;
			while (!rounded && j < len) {
				rounded = bin[j++];
			}
		}

		j = lastBit + 1;
		while (rounded && --j >= 0) {
			(bin[j] = !bin[j] - 0) && (rounded = 0);
		}
	}
	i = i - 2 < 0 ? -1 : i - 3;
	while (++i < len && !bin[i]);
	(exp = 128 - i) >= -126 && exp <= 127
		? ++i
		: exp < -126 && ((i = 255), (exp = -127));
	(intPart || status !== 0) &&
		((exp = 128),
		(i = 129),
		status == -Infinity ? (signal = 1) : status !== status && (bin[i] = 1));

	n = Math.abs(exp + 127);
	exponent = 0;
	j = 0;
	while (j < 8) {
		exponent += n % 2 << j;
		n >>= 1;
		j++;
	}

	var mantissa = 0;
	n = i + 23;
	for (; i < n; i++) {
		mantissa = (mantissa << 1) + bin[i];
	}
	return ((signal ? 0x80000000 : 0) + (exponent << 23) + mantissa) | 0;
}

// Remplace les charactères non ascii en code unicode
function toUnicode(str) {
	return str
		.split("")
		.map(function(value, index, array) {
			let temp = value.charCodeAt(0);
			if (temp > 127 || temp <= 0x20 || temp === 0x7f) {
				let ret = "\\u";
				for (let i = 0; i < 4 - temp.toString(16).length; i++)
					ret += "0";
				return ret + temp.toString(16);
			}
			return value;
		})
		.join("");
}

// Convertis un nombre en un string binaire avec la longueur n
function numToBin(num, n) {
	if (num >= 0) {
		let ret = num.toString(2);
		while (ret.length < n) {
			ret = "0" + ret;
		}
		return ret;
	} else {
		let ret = (Math.abs(num) - 1).toString(2);
		while (ret.length < n) {
			ret = "0" + ret;
		}
		let newret = "";
		for (var i = 0; i < ret.length; i++) {
			if (ret[i] === "0") newret += 1;
			else newret += 0;
		}
		return newret;
	}
}

// Transforme un nombre entier en un réel avec la meme représentation binaire
function decodeFloat(string) {
	let buffer = new ArrayBuffer(4);
	let intView = new Int32Array(buffer);
	let floatView = new Float32Array(buffer);
	intView[0] = parseInt(string, 2);
	return floatView[0];
}

// Simple sleep
function sleep(milliseconds) {
	var start = new Date().getTime();
	for (var i = 0; i < 1e7; i++) {
		if (new Date().getTime() - start > milliseconds) {
			break;
		}
	}
}

function uintToInt(uint, nbit) {
	nbit = +nbit || 32;
	if (nbit > 32)
		throw new RangeError("uintToInt only supports ints up to 32 bits");
	uint <<= 32 - nbit;
	uint >>= 32 - nbit;
	return uint;
}
