import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from '../core/auth.service';
import { MatTableDataSource } from '@angular/material';
import { DataSource } from '@angular/cdk/table';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  users = this.auth.getUsers();

  userDetails = {
    uid: '',
    displayName: '',
    email: '',
    isOnline:'',
    isAdmin:'',

  }
 
  displayedColumns = ['uid', 'displayName', 'email', 'isOnline', 'isAdmin','action'];
  dataSource = new UserDataSource(this.auth);


  constructor(private route: ActivatedRoute,private afs: AngularFirestore,
		public auth: AuthService) { }

  ngOnInit() {
  }
  changerRole(uid){
    this.auth.changerRole(uid);
  

  }

}

export class UserDataSource extends DataSource<any> {
 
  constructor(private user: AuthService) {
  super()
  }
 
  connect() {
    return this.user.afs.collection('users', ref => ref.orderBy('displayName')).valueChanges();
  }
 
  disconnect() {
 
  }
}
