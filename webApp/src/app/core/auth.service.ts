import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { auth } from 'firebase/app';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';

import { Observable, of } from 'rxjs';
import { switchMap, first} from 'rxjs/operators';
import { AngularFireDatabase,AngularFireList } from '@angular/fire/database';


interface User {
  uid: string;
  isOnline:boolean;
  isAdmin?:boolean;
  email: string;
  photoURL?: string;
  displayName?: string;
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  user: Observable<User>;
  users =[];

  constructor(
    private afAuth: AngularFireAuth,
    private db : AngularFireDatabase,
    public afs: AngularFirestore,
    private router: Router
  ) {


      //// Get auth data, then get firestore user document || null
      this.user = this.afAuth.authState.pipe(
        switchMap(user => {
          if (user) {
            return this.afs.doc<User>(`users/${user.uid}`).valueChanges()
          } else {
            return of(null)
          }
        })
      )
    }

  googleLogin() {
    const provider = new auth.GoogleAuthProvider()
    //auth().currentUser.sendEmailVerification;
    return this.oAuthLogin(provider);
  }

  private oAuthLogin(provider) {
    return this.afAuth.auth.signInWithPopup(provider)
      .then((credential) => {
        this.updateUserData(credential.user)
      })
  }


  private updateUserData(user) {
    // Sets user data to firestore on login

    const userRef: AngularFirestoreDocument<any> = this.afs.doc(`users/${user.uid}`);

    const data: User = {
      uid: user.uid,
      email: user.email,
      displayName: user.displayName,
      photoURL: user.photoURL,
      isOnline:true,
      isAdmin: false

    }

    

    return userRef.set(data, { merge: true })

  }
  getUsers(){
    
    this.afs.firestore.collection('users').get().then(
      (snapshot)=> {
        snapshot.docs.forEach(doc=>{
          this.users.push(doc.data());
        })
      }
    );


    //this.afs.collection('users', ref => ref.orderBy('displayName')).valueChanges();

  
    
    return this.users;
    
  }
  getUser() {
    return this.user.pipe(first()).toPromise();
  }
  private setUserOffline(user) {
    
    const userRef: AngularFirestoreDocument<any> = this.afs.doc(`users/${user.uid}`);
    return userRef.set({isOnline: false}, { merge: true })

  }
 
  private setUserOnline(user) {
    // Sets user data to firestore on login

    const userRef: AngularFirestoreDocument<any> = this.afs.doc(`users/${user.uid}`);
    return userRef.set({isOnline: true}, { merge: true })

  }
  getUserName(): String  {
    return auth().currentUser.displayName;
  }
  getUserId(): String  {
    return auth().currentUser.uid;
  }
  getUserPhoto(): String  {
    return auth().currentUser.photoURL;
  }


  signUp(email,password,nom,isAdmin,photoURL){
    return  auth()
    .createUserWithEmailAndPassword(email, password)
    .then(
      docRef => {
      console.log('Successfully signed up!');
      this.afs.doc(`users/${docRef.user.uid}`).set(
        {
          uid:docRef.user.uid,
          email: email,
          displayName: nom,
          photoURL: photoURL,
          isOnline:true,
          isAdmin: isAdmin
    
        }
      );

      auth().currentUser.sendEmailVerification().then(function() {
        // Email sent.
      }).catch(function(error) {
        // An error happened.
      });

      this.router.navigate(['/login'])

    })
    .catch(error => {
      console.log('Something is wrong:', error.message);
    });    



  }

  login(email,password){
    auth()
        .signInWithEmailAndPassword(email, password)
        .then(res => {
          console.log('Successfully signed in!');
          this.setUserOnline(res.user);
        })
        .catch(err => {
          console.log('Something is wrong:',err.message);
        });
  }


  signOut() {
    
    this.setUserOffline(auth().currentUser);
    this.afAuth.auth.signOut().then(() => {
        this.router.navigate(['/']);
    });
  }
  changerRole(user){
    const userRef= this.afs.doc("users/"+user);
     userRef.get().toPromise().then(function(doc) {
      if (doc.exists) {
          if(doc.data()['isAdmin']){
            userRef.set({isAdmin: false}, { merge: true })
          }
          else{
            userRef.set({isAdmin: true}, { merge: true })
          }
          

      } else {
          // doc.data() will be undefined in this case
          console.log("No such document!");
      }
      }).catch(function(error) {
      console.log("Error getting document:", error);
  });

    
    
  }

}