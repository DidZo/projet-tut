import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestCityComponent } from './test-city.component';

describe('TestCityComponent', () => {
  let component: TestCityComponent;
  let fixture: ComponentFixture<TestCityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestCityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestCityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
