import { Component, OnInit, OnDestroy } from "@angular/core";

import * as p5 from "p5";
import { TouchSequence } from "selenium-webdriver";

//#region Methodes importées de js
declare function encode(objet): any;
declare function decode(chunk): any;
declare function decompress(data): any;
declare function compress(data): any;

declare function sleep(milliseconds): any;
declare function numToBin(num, n): any;

declare var CityObject: any;
declare var Action: any;
//#endregion

@Component({
	selector: "app-test-city",
	templateUrl: "./test-city.component.html",
	styleUrls: ["./test-city.component.scss"]
})
export class TestCityComponent implements OnInit, OnDestroy {
	message: string = "";
	nbrEnvois: number = 1;
	ws: WebSocket;

	canvas_p5: any;

	gameObjectList = [];
	nodeList = [];

	values = {
		action: "",
		type: "",
		id_obj: "88",

		variante: "250",
		posx: "12.3",
		posy: "8.69",
		angle: "12",
		vitesse: "50",
		longueur: "2500",
		rayon: "50",
		zoom: "",
		sens_unique: false,

		id_start: "",
		id_finish: "",

		debug: false
	};
	launchApp: () => any;

	constructor() {

	}

	envoyerMessage() {
		for (var i = 0; i < this.nbrEnvois; i++) {
			const obj = {
				action: parseInt(this.values.action),
				type: parseInt(this.values.type),
				id_obj: parseInt(this.values.id_obj),

				variante: parseInt(this.values.variante),
				posx: parseFloat(this.values.posx),
				posy: parseFloat(this.values.posy),
				angle: parseInt(this.values.angle),
				vitesse: parseInt(this.values.vitesse),
				longueur: parseInt(this.values.longueur),
				rayon: parseInt(this.values.rayon),
				zoom: parseInt(this.values.zoom),
				sens_unique: this.values.sens_unique,

				id_start: parseInt(this.values.id_start),
				id_finish: parseInt(this.values.id_finish)
			};

			let comp = encode(obj);
			if (this.values.debug) comp = "debug:" + comp;
			this.ws.send(comp);

			sleep(5);
		}
	}

	ngOnInit() {
		const sketch = s => {
			let spriteList = {};

			let cursor_target = null;

			//#region CLASS

			class NodeSelector {
				static p1: any = null;
				static p2: any = null;

				static draw(cam: Camera) {
					s.fill(255, 255, 0);
					if (this.p1 != null)
						s.circle(
							this.p1.position.x + cam.position.x,
							this.p1.position.y + cam.position.y,
							25
						);
					if (this.p2 != null)
						s.circle(
							this.p2.position.x + cam.position.x,
							this.p2.position.y + cam.position.y,
							25
						);
				}

				static assign(road) {
					if (this.p1 == null) {
						this.p1 = road;
						return;
					}
					this.p2 = road;
					var msg = {
						action: Action.UPDATE,
						type: CityObject.CAR_WITH_PATH,
						id_start: component.nodeList.indexOf(this.p1),
						id_finish: component.nodeList.indexOf(this.p2)
					};
					console.log(msg);
					let coded = encode(msg);
					component.ws.send(coded);
					console.log(coded);
					this.p1 = null;
					this.p2 = null;
				}
			}

			class Node {
				id: 0;
				position = {
					x: 0,
					y: 0
				};

				constructor(id, x, y) {
					this.id = id;
					this.position = {
						x: x * (1.0/0.24),
						y: y * (1.0/0.24)
					};
				}
			}

			class Camera {
				zoom: 1;
				position = {
					x: 0,
					y: 0
				};

				constructor() {
					this.zoom = 1;
					this.position = {
						x: 0,
						y: 0
					};
				}
			}

			class Car {
				id: 0;
				user: boolean = false;

				position = {
					x: 0,
					y: 0
				};

				variante: 0;
				angle: 0;
				vitesse: 0;

				disparition: any = 0;

				constructor(id, v, x, y, a, vi, u) {
					this.id = id;
					this.position.x = x * (1/0.24);
					this.position.y = y * (1/0.24);
					this.variante = v;
					this.angle = a;
					this.vitesse = vi;
					this.user = u;
				}

				draw(camera) {
					this.disparition += s.deltaTime;
					if(this.disparition > 1000)
						component.gameObjectList.splice(component.gameObjectList[component.gameObjectList.indexOf(this)], 1); 

					/*
						- La boucle du serveur envoie 10 msg par secondes (msgps)
						- On a un appli a 60 fps (framerate)
						- Ducoup on veut que la voiture avance toute seul pdt 6 frames (framerate / msgps)
						- Elle a une vitesse v qui correspond à la distance qu'elle va parcourir durant tic 
						- on veut qu'elle parcour la distance coresspondant à v pdt un tic
						- Alors à chaque frame on fait : position += v / (framerate / msgps)
					*/

					let msgps = 10;
					let pos = s.createVector(this.position.x, this.position.y);
					let distance = this.vitesse / (s.frameRate() / msgps);

					let vec = s.createVector(distance, 0);

					vec.rotate(this.angle / 1000);

					pos.add(vec);

					this.position.x = pos.x;
					this.position.y = pos.y;

					s.push();

					s.rectMode(s.CENTER);
					s.imageMode(s.CENTER);

					s.translate(
						this.position.x + camera.position.x,
						this.position.y + camera.position.y
					);

					s.scale(0.1, 0.1);

					s.rotate((s.PI) / 2 + this.angle / 1000);


					// s.fill(this.variante);
					// s.rect(0, 0, 20, 35);
					
					s.image(spriteList["voiture" + this.variante], 0, 0);

					s.pop();
				}
			}

			class Pedestrian {
				position = {
					x: 0,
					y: 0
				};

				variante: 0;
				angle: 0;
				vitesse: 0;

				constructor(v, x, y, a, vi) {
					this.position.x = x;
					this.position.y = y;
					this.variante = v;
					this.angle = a;
					this.vitesse = vi;
				}

				draw(camera) {
					s.push();

					s.translate(
						this.position.x + camera.position.x,
						this.position.y + camera.position.y
					);
					s.rotate((this.angle * s.PI) / 180);

					s.fill(this.variante);
					s.ellipse(0, 0, 10, 10);
					s.line(0, 0, 0, 10);

					s.pop();
				}
			}

			//#endregion

            let cam = new Camera();
            
            s.getMouseWorldCoord = () =>{
                return {
                    x: (s.mouseX - s.width/2) / cam.zoom - cam.position.x, 
                    y: (s.mouseY - s.height/2) / cam.zoom - cam.position.y
                };
            }

			s.preload = () => {
				// preload code
				spriteList["voiture0"] = s.loadImage("assets/img/Voiture0.png");
				spriteList["voiture1"] = s.loadImage("assets/img/Voiture1.png");
				spriteList["voiture2"] = s.loadImage("assets/img/Voiture2.png");
				spriteList["bg"] = s.loadImage("assets/img/bg.png");
				spriteList["fg"] = s.loadImage("assets/img/fg.png");
				spriteList["fleche"] = s.loadImage("assets/img/fleche.png");
			};

			s.windowResized = () => {
				let navSize = document.getElementsByClassName("header-app")[0]
					.clientHeight;
				s.resizeCanvas(s.windowWidth, s.windowHeight - navSize, false);
				document.getElementById("sketch-holder").style.top =
					navSize + "px";
			};

			s.setup = () => {
				var canvas = s.createCanvas(
					s.windowWidth,
					s.windowHeight -
						document.getElementsByClassName("header-app")[0]
							.clientHeight
				);
				canvas.parent("sketch-holder");
				s.windowResized();

				//spriteList["bg"].resize(0, 0);

				cam.position.x = -1000;
				cam.position.y = -1000;
			};

			s.drawSelected = () => {
				let smaller = 25;
				let target = null;
				this.nodeList.forEach(function(obj) {
					// let dist = s.dist(
					// 	s.mouseX,
					// 	s.mouseY,
					// 	(obj.position.x + cam.position.x) * cam.zoom + s.width / 2,
					// 	(obj.position.y + cam.position.y) * cam.zoom + s.height / 2
                    // );

                    let dist = s.dist(
						(s.mouseX - s.width/2) / cam.zoom - cam.position.x,
						(s.mouseY - s.height/2) / cam.zoom - cam.position.y,
						obj.position.x,
						obj.position.y
                    );
                    
					if (dist < smaller) {
						smaller = dist;
						target = obj;
					}
				});
				if (target != null) {
					s.fill(255, 0, 0);
					s.circle(
						target.position.x + cam.position.x,
						target.position.y + cam.position.y,
						25
					);
					cursor_target = target;
				} else {
					cursor_target = null;
				}
			};

			s.draw = () => {
				s.background(200);
                
                s.translate(s.width/2, s.height/2);
				s.scale(cam.zoom);
				
				s.imageMode(s.CORNER);
				s.image(spriteList["bg"], cam.position.x, cam.position.y);
            

				// On dessine tout les objets
				this.gameObjectList.forEach(obj => {
					obj.draw(cam);
				});

				s.image(spriteList["fg"], cam.position.x, cam.position.y);

				
				// On dessine les  fleches
				this.gameObjectList.forEach(obj => {
					if(obj.user){
						s.push();

						s.rectMode(s.CENTER);
						s.imageMode(s.CENTER);

						s.translate(
							obj.position.x + cam.position.x,
							obj.position.y + cam.position.y
						);

						s.scale(0.2, 0.2)
						if(obj.user)
							s.image(spriteList["fleche"], 0, -300*(1.0/0.24));

						s.pop();
					}
				});
				



				// On dessine tout les noeuds
				this.nodeList.forEach(obj => {
                    s.fill(255,200,0);
					s.circle((obj.position.x + cam.position.x), (obj.position.y + cam.position.y), 40);
                });

				s.drawSelected();
                NodeSelector.draw(cam);

				if (s.keyIsPressed) {
					switch (s.key) {
						case "s":
							cam.position.y -= 5;
							break;
						case "z":
							cam.position.y += 5;
							break;
						case "d":
							cam.position.x -= 5;
							break;
						case "q":
							cam.position.x += 5;
							break;
					}
				}

				s.zoomTouchScreen();

			};

			s.checkDezoom = (delta, mult) => {
				if(delta > 0){
					/*
						- On calcul la distance entre le haut et la camera
						- On transpose cette distance en vue réelle (en multipliant par le zoom)
						- Si elle est plus petite que la moitié de la hauteur de l'écran on ne peux plus dézoomer
					*/
					// Haut == == == == == == == == == == == == == == == == == == == == == == == == == ==
					let dist = s.abs(cam.position.y);
					dist *= (cam.zoom*mult);
					if(dist < s.height/2)
						return false;
					// Gauche == == == == == == == == == == == == == == == == == == == == == == == == == ==
					dist = s.abs(cam.position.x);
					dist *= (cam.zoom*mult);
					if(dist < s.width/2)
						return false;
					// Bas == == == == == == == == == == == == == == == == == == == == == == == == == ==
					dist = spriteList["bg"].height - s.abs(cam.position.y);
					dist *= (cam.zoom*mult);
					if(dist < s.height/2)
						return false;
					// Droite == == == == == == == == == == == == == == == == == == == == == == == == == ==
					dist = spriteList["bg"].width - s.abs(cam.position.x);
					dist *= (cam.zoom*mult);
					if(dist < s.width/2)
						return false;

				}
				return true;
			}

			s.mouseWheel = event => {
				/*
					Si la distance entre le bord de l'image et le milieu de l'écran est plus petite que la moitié de la hauteur de l'écran on ne peux plus dézoomer
				*/
				
				if(!s.checkDezoom(event.delta, 0.9)) return;

				if (event.delta > 0) cam.zoom *= 0.9;
                else cam.zoom *= 1.1;

                return false;
			};

			let pdist = 0;
			s.zoomTouchScreen = () => {
				if (s.touches.length < 2) {
					pdist = 0;
					return;
				}

				// Zoom tactile
				if (s.touches.length >= 2) {
					let p0 = s.createVector(s.touches[0].x, s.touches[0].y);
					let p1 = s.createVector(s.touches[1].x, s.touches[1].y);
                    let dist = p0.dist(p1);
                    
                    let diff = s.abs(dist - pdist);
                    if(diff > 90)
						diff = 90;

					if (pdist != 0) {
						
						if(!s.checkDezoom(-(dist - pdist), 1.0 - 0.01 * diff)) return;

						if (pdist < dist)
							cam.zoom *= 1.0 + 0.01 * diff;
						else if (pdist > dist)
							cam.zoom *= 1.0 - 0.01 * diff;
					}

					// s.fill(255, 0, 0);
					// s.text(dist, 10, 10);

					pdist = dist;
				}
			};

			s.mouseDragged = () => {
				if (s.touches.length >= 2) return;

				let offsetX = (s.pmouseX - s.mouseX) / cam.zoom;
				let offsetY = (s.pmouseY - s.mouseY) / cam.zoom

				/*
					- On prend la disatance entre le haut de l'image et le centre de la camera
					- On mutliplie vette distance par le zoom
					- Si la distance moins le decallage est plus petit que la moitié de la hauteur de l'écran on ne peux plus décaller DANS CE SENS
				*/
				if(offsetY < 0){
					// Haut == == == == == == == == == == == == == == == == == == == == == == == == == ==
					let dist = s.abs(cam.position.y);
					dist *= cam.zoom;
					if(dist+offsetY > s.height/2)
						cam.position.y -= offsetY;
				}else{
					// Bas == == == == == == == == == == == == == == == == == == == == == == == == == ==
					let dist = spriteList["bg"].height - s.abs(cam.position.y);
					dist *= cam.zoom;
					if(dist-offsetY > s.height/2)
						cam.position.y -= offsetY;
				}
				if(offsetX < 0){
					// Gauche == == == == == == == == == == == == == == == == == == == == == == == == == ==
					let dist = s.abs(cam.position.x);
					dist *= cam.zoom;
					if(dist+offsetX > s.width/2)
						cam.position.x -= offsetX;
				}else{
					// Droite == == == == == == == == == == == == == == == == == == == == == == == == == ==
					let dist = spriteList["bg"].width - s.abs(cam.position.x);
					dist *= cam.zoom;
					if(dist-offsetX > s.width/2)
						cam.position.x -= offsetX;
				}

			};

			s.mouseClicked = event => {
				if (event.button == 0) {
					if (cursor_target != null) {
						NodeSelector.assign(cursor_target);
					}
				}
			};

			s.addObject = obj => {
				if (
					parseInt(obj.action) == Action.UPDATE &&
					parseInt(obj.type) == CityObject.CAR
				) {
					if(obj.user){
						obj.variante = 1;
					}
					let found = false;
					for(let i = 0; i < component.gameObjectList.length; i++){
						if(component.gameObjectList[i].id == obj.id_obj){
							component.gameObjectList[i] = 
								new Car(
									obj.id_obj,
									obj.variante,
									obj.posx,
									obj.posy,
									obj.angle,
									obj.vitesse/10,
									obj.user
								); 
							found = true;
							return;

						}
					}
					if(!found)
						this.gameObjectList.push(new Car(
							obj.id_obj,
							obj.variante,
							obj.posx,
							obj.posy,
							obj.angle,
							obj.vitesse/10,
							obj.user
						)); 

				} else if (
					parseInt(obj.action) == Action.UPDATE &&
					parseInt(obj.type) == CityObject.PEDESTRIAN
				) {
					if(component.gameObjectList[obj.id_obj] == null){
						component.gameObjectList[obj.id_obj] = new Pedestrian(
							obj.variante,
							obj.posx,
							obj.posy,
							obj.angle,
							obj.vitesse
						);
					}else{
						component.gameObjectList[obj.id_obj].angle = obj.angle;
						component.gameObjectList[obj.id_obj].posx = obj.posx;
						component.gameObjectList[obj.id_obj].posy = obj.posy;
						component.gameObjectList[obj.id_obj].vitesse = obj.vitesse;
					}
				} else if (
					parseInt(obj.action) == Action.UPDATE &&
					parseInt(obj.type) == CityObject.NODE
				) {
					component.nodeList[obj.id_obj] = new Node(
						obj.id,
						obj.posx,
						obj.posy
					);
				}
				//console.log(this.gameObjectList);
			};
		};
		var component = this;

		this.launchApp = function() {
			component.canvas_p5 = new p5(sketch);

			this.ws = new WebSocket("ws://" + document.location.host + "/city");

			this.ws.onopen = function() {
				console.log("onopen");
			};
			this.ws.onclose = function() {
				component.message = "Lost connection";
				console.log("onclose");
			};
			this.ws.onmessage = function(message) {
				let decoded = decode(message.data);
				component.canvas_p5.addObject(decoded);
				//console.log(decoded);
			};
			this.ws.onerror = function(error) {
				console.log("onerror " + error);
				console.log(error);
			};
		};

		this.launchApp();
	}

	ngOnDestroy() {
		this.ws.close();
		this.canvas_p5.remove();
	}
}
