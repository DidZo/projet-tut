import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestP5Component } from './test-p5.component';

describe('TestP5Component', () => {
  let component: TestP5Component;
  let fixture: ComponentFixture<TestP5Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestP5Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestP5Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
