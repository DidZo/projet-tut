import { Component, OnInit } from "@angular/core";
import * as p5 from "p5";

@Component({
	selector: "app-test-p5",
	templateUrl: "./test-p5.component.html",
	styleUrls: ["./test-p5.component.scss"]
})
export class TestP5Component implements OnInit {
	posX: number = 0;
	posY: number = 0;

	constructor() {}

	ngOnInit() {
		this.motion();
	}

	motion() {
		const xStart = 0;
		const yStart = 250;

		let posX;
		let posY;

		let direction = "right";

		const sketch = s => {
			s.preload = () => {
				// preload code
			};

			var zoom = 1.0;

			s.setup = () => {
				var canvas = s.createCanvas(
					s.displayWidth * 0.75,
					s.displayHeight * 0.75
				);
				canvas.parent("sketch-holder");
			};

			s.draw = () => {
				s.background(128);
                zoom += 0.005;
                
                s.fill(255,120,0);
				let display = s.touches.length + " touches";
                s.text(display, 5, 10);
                                    

				s.translate(s.width / 2, s.height / 2);
				s.scale(zoom);

				s.fill(255, 128, 0);
				s.rect(100, 100, 50, 50);
				s.rect(20, 50, 25, 25);

			};

			s.mouseWheel = event => {
				zoom += event.delta * 0.01;
			};

			// Zoom tactile global var
			let ptouches = [{ x: 0, y: 0, id: 0 }, { x: 0, y: 0, id: 1 }];

			s.touchMoved = () => {
                s.text(s.touches[0].x + ", " + s.touches[0].y, 5, 25);


				// Zoom tactile
				if (s.touches.length >= 2) {
					let p0 = s.createVector(s.tocuhes[0].x, s.tocuhes[0].y);
					let p1 = s.createVector(s.tocuhes[1].x, s.tocuhes[1].y);
                    let dist = p0.dist(p1);
                    

					s.fill(255, 0, 0);
					s.text(dist, 10, 10);

					ptouches = s.touches;
				}

				return false;
			};
		};

		let canvas = new p5(sketch);
	}
}
