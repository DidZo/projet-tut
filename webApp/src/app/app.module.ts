import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TestCityComponent } from './test-city/test-city.component';
import { HomeComponent } from './home/home.component';
import { ChatComponent } from './chat/chat.component';
import { TestP5Component } from './test-p5/test-p5.component';
import { MatToolbarModule, MatSidenavModule, MatListModule, MatButtonModule, MatIconModule, MatInputModule, MatFormFieldModule, MatRippleModule, MatOptionModule, MatSelectModule, MatCheckbox, MatCheckboxModule, MatTabsModule, MatCardModule, MatTableModule } from "@angular/material";
import {FlexLayoutModule} from '@angular/flex-layout';

import { FormsModule } from "@angular/forms";
import { AnimateOnScrollModule } from 'ng2-animate-on-scroll';

import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { InfoComponent } from './info/info.component';
import { CreditsComponent } from './credits/credits.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FooterComponent } from './footer/footer.component';

import { CoreModule } from './core/core.module';
 
import {HttpClientModule} from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { ContactComponent } from './contact/contact.component';
import { ContactFormComponent } from './contact-form/contact-form.component';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AuthService } from './core/auth.service';
import { AngularFireModule } from '@angular/fire';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { SignUpFormComponent } from './sign-up-form/sign-up-form.component';
import { ClientGuard } from './core/client.guard';
import { AdminGuard } from './core/admin.guard';
import { ChatService } from './chat.service';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { UsersComponent } from './users/users.component';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';

@NgModule({
  declarations: [
    AppComponent,
    TestCityComponent,
    TestP5Component,
    HomeComponent,
    ChatComponent,
    InfoComponent,
    CreditsComponent,
    FooterComponent,
    ContactComponent,
    ContactFormComponent,
    UserProfileComponent,
    SignUpFormComponent,
    UsersComponent,
    
  ],
  imports: [
    MatTabsModule,
    CoreModule,
    ScrollingModule,
    MatTableModule,
    AngularFireAuthModule,
    AngularFirestoreModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    AnimateOnScrollModule.forRoot(),
    BrowserAnimationsModule,
    MatToolbarModule,
    MatSidenavModule,
    MatListModule,
    MatIconModule,
    MatButtonModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    HttpClientModule,
    MDBBootstrapModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatRippleModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatRippleModule,
    MatOptionModule,
    MatSelectModule,
    MatCardModule,
    MatCheckboxModule,
    AngularFireDatabaseModule,
  AngularFireModule.initializeApp({
    apiKey: "AIzaSyB2az75P6kW8J3hzDYOGmNxSgv3uxo6HKo",
  authDomain: "belformatique-13663.firebaseapp.com",
  databaseURL: "https://belformatique-13663.firebaseio.com",
  projectId: "belformatique-13663",
  storageBucket: "belformatique-13663.appspot.com",
  messagingSenderId: "412229960660",
  appId: "1:412229960660:web:60d6bdc7a874a38bf90c41"
  
  }),
  MatPaginatorModule,
  MatSortModule,

    
  ],
  providers: [
    AuthService,
    AdminGuard,
    ClientGuard,
    ChatService,
    {provide: LocationStrategy, useClass: HashLocationStrategy}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

