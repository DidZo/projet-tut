import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from "./home/home.component";
import { TestCityComponent } from './test-city/test-city.component';
import { TestP5Component } from './test-p5/test-p5.component';
import { ChatComponent } from './chat/chat.component';
import { ContactComponent } from './contact/contact.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { AdminGuard } from './core/admin.guard';
import { ClientGuard } from './core/client.guard';
import { UsersComponent } from './users/users.component';


const routes: Routes = [
    { path: "", component: HomeComponent },
    { path: "test-city", component: TestCityComponent },
    { path: "test-p5", component: TestP5Component },
    { path: "chat/:id", component: ChatComponent },
    { path: "login", component: UserProfileComponent },
    { path: "utilisateurs", component: UsersComponent, canActivate : [AdminGuard] },
    { path: "contact", component: ContactComponent ,canActivate: [ClientGuard] },
    { path: 'not-found', component: HomeComponent },
    { path: '**', redirectTo: 'not-found' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
