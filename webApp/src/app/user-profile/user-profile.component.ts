import { Component, OnInit, HostListener } from '@angular/core';
import { AuthService } from '../core/auth.service';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@Component({
  selector: 'user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss'],
})
export class UserProfileComponent {

  registerForm: FormGroup;
  loginForm: FormGroup;

  hide = true;
  
  user= this.auth.getUser();
    constructor(private fb: FormBuilder, private auth: AuthService) {
  
    this.registerForm = fb.group({
      'nom': ['', Validators.required],
      'email': ['', Validators.compose([Validators.required, Validators.email])],
      'motDePasse': ['', Validators.required],
      });


      this.loginForm = fb.group({
        'email': ['', Validators.compose([Validators.required, Validators.email])],
        'motDePasse': ['', Validators.required],
        });
      //this.user = auth.getUser();
    }
  
    signUp() {
      console.log("hi there");
     this.auth.signUp(
      this.registerForm.get('email').value,
      this.registerForm.get('motDePasse').value,
      this.registerForm.get('nom').value,
      false,      
      "photo"
     );
     
    }
    login(email: string, password: string) {
        this.auth.login(this.loginForm.get('email').value,this.loginForm.get('motDePasse').value);
    }

}

