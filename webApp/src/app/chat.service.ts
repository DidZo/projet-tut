import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AuthService } from './core/auth.service';
import { Router } from '@angular/router';
import * as firebase from 'firebase/app';


@Injectable({
  providedIn: 'root'
})
export class ChatService {

   currentUser = this.auth.getUser();

  constructor(private afs: AngularFirestore,
    private auth: AuthService,
    private router: Router) { }

    getChat(chatId){
      
      return this.afs.collection('chats').doc(chatId).valueChanges();
    }
    async sendMessage(chatId, content) {
      const { uid } = await this.auth.getUser();
  
      const data = {
        uid,
        content,
        createdAt: Date.now(),
        userName: this.auth.getUserName(),
        photoUrl: this.auth.getUserPhoto()
      };
  
      if (uid) {
        const ref = this.afs.collection('chats').doc(chatId);
        return ref.update({
          messages: firebase.firestore.FieldValue.arrayUnion(data)
        });
      }
    }

    async deleteMessage(chat, msg) {
      const { uid } = await this.auth.getUser();
  
      const ref = this.afs.collection('chats').doc(chat.id);
        // Allowed to delete
        delete msg.user;
        return ref.update({
          messages: firebase.firestore.FieldValue.arrayRemove(msg)
        });
      
    }
    getUserId(){
      return this.auth.getUserId();
    }

    async create(user) {


      const { uid } = await this.auth.getUser();

      // si l'identidiant uid user existe
      var usersRef = this.afs.collection('chats').doc(uid+"_"+user);

        usersRef.get().toPromise()
          .then( (docSnapshot) => {
            if (docSnapshot.exists) {
              // on affiche le chat
              this.router.navigate(['chat/'+uid+"_"+user]);
              
  
            } else {
              //sinon on essaye de trouver l'id user uid
              var usersRef2 = this.afs.collection('chats').doc(user+"_"+uid);

              usersRef2.get().toPromise()
              .then( (docSnapshot2) => {

            if (docSnapshot2.exists) {
              this.router.navigate(['chat/'+user+"_"+uid]);
             
  
            } else {
              //s'il n'existe pas on crée le chat
              var citiesRef = this.afs.collection("chats");
              citiesRef.doc(uid+"_"+user).set({
                from:uid,
                to:user,
                createdAt: Date.now(),
                messages: [],
              
              });
              this.router.navigate(['chat/'+uid+"_"+user]);
              
            }
        });


            }
        });
        setTimeout(

          function(){ 
          location.reload(); 
          }, 1000);
      

}
}