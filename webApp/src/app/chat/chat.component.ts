import { Component, OnInit } from "@angular/core";
import { Observable } from 'rxjs';
import { ChatService } from '../chat.service';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from '../core/auth.service';
import { map } from 'rxjs/operators';

@Component({
	selector: "app-chat",
	templateUrl: "./chat.component.html",
	styleUrls: ["./chat.component.scss"]
})
export class ChatComponent implements OnInit {
	chat$: Observable<any>;
	newMsg: string;
	chatId;
	users;

	constructor(public cs: ChatService,
		private route: ActivatedRoute,
		public auth: AuthService) {
	}

	

	ngOnInit() {
		this.getAllUsers();
		this.chatId = this.route.snapshot.paramMap.get('id');
		this.chat$ = this.cs.getChat(this.chatId);
		this.scrollBottom();
	}

	getAllUsers() {
		this.users=this.auth.getUsers();

	  }

	
	submit(){
		if (!this.newMsg) {
			return alert('you need to enter something');
		  }
		  this.cs.sendMessage(this.chatId,this.newMsg);
		  this.newMsg = '';
		  this.scrollBottom();
		
	}
	trackByCreated(i, msg) {
		return msg.createdAt;
	  }
	
	  private scrollBottom() {
		setTimeout(() => window.scrollTo(0, document.body.scrollHeight), 500);
	  }


}
