/**
 * @file CPPtoJS.h
 * @brief Compacter les messages de communication entre le serveur et le client.
 * @author Etienne.T
 * @version 0.1
 * @date 14 septembre 2019
 *
 * Pas encore de description détaillé.
 *
 */

#ifndef CPPTOJS_ENCODEUR_H
#define CPPTOJS_ENCODEUR_H

#include <iostream>
#include <string>
#include <bitset>

using namespace std;


// DEFINITION DES TYPES = = = = = = = = = = = = = = = =

typedef unsigned short TYPE_ID;
typedef float TYPE_POS;
typedef short TYPE_ANGLE;
typedef char  TYPE_VITESSE;
typedef char  TYPE_VARIANTE;
typedef short TYPE_ZOOM;

//= = = = = = = = = = = = = = = = = = = = = = = = = = =

class Encodeur {

public:

    /**
     * Constantes servant à identifier l'action à réaliser
     */
    enum Action : unsigned char {
        DELETE = 0b01,
        UPDATE = 0b10
    };

    /**
     * Constantes servant à identifier le type d'objet sur lequel on travaille
     */
    enum CityObject : unsigned char {
        CAMERA        = 0b000,
        NODE          = 0b001,

        CAR           = 0b011,
        PEDESTRIAN    = 0b100,

        CAR_WITH_PATH = 0b110
    };

    /**
     * Encode l'info "supprimer un objet"
     *
     * @param id Identifiant de l'objet à supprimer
     *
     * @return string de l'info encodée
     */
    static string Delete(TYPE_ID id);

    static string UpdateNode(TYPE_ID id, TYPE_POS posx, TYPE_POS posy);

    /**
     * Encode l'info "modifier une voiture"
     *
     * @param id Identifiant d'instance de l'objet à modifier
     * @param variante Variante de voiture à modifier
     * @param posx Position X de l'objet à modifier
     * @param posy Position Y de l'objet à modifier
     * @param angle Angle de rotation de l'objet à modifier
     * @param vitesse Vitesse de la voiture à modifier
     *
     * @return string représentant l'info encodée
     */
    static string UpdateCar(TYPE_ID id, TYPE_VARIANTE variante, TYPE_POS posx, TYPE_POS posy, TYPE_ANGLE angle, TYPE_VITESSE vitesse, bool user);

    /**
     * Encode l'info "modifier un piéton"
     *
     * @param id Identifiant d'instance de l'objet à modifier
     * @param variante Variante du piétons à modifier
     * @param posx Position X de l'objet à modifier
     * @param posy Position Y de l'objet à modifier
     * @param angle Angle de rotation de l'objet à modifier
     * @param vitesse Vitesse de la voiture à modifier
     *
     * @return string représentant l'info encodée
     */
    static string UpdatePedestrian(TYPE_ID id, TYPE_VARIANTE variante, TYPE_POS posx, TYPE_POS posy, TYPE_ANGLE angle, TYPE_VITESSE vitesse);

    /**
     * Decode une info
     *
     * @param chunk string représentant k'info à décoder
     *
     * @return string de l'info décodé
     */
    static string Decode(string chunk);

private:

    Encodeur()= default;

    union Flint {
        int i;
        long l;
        float f;
    };

    static string Compress(string data);
    static string Decompress(string data);

    static string DecodeCarPedestrian(string data);
    static string DecodeCamera(string data);
    static string DecodeCarWithPath(string data);

    static string EncodeCarPedestrian(Action action, TYPE_ID id, CityObject type, TYPE_VARIANTE variante, TYPE_POS posx, TYPE_POS posy, TYPE_ANGLE angle, TYPE_VITESSE vitesse, bool user);

};


#endif //CPPTOJS_ENCODEUR_H