# CPPtoJS documentation

## C++ 
### Ouvrir une connexion

**La bibliothèque utilisé est [seasocks](https://github.com/mattgodbolt/seasocks)**  
Il faut créer une structure qui étend `seasocks::WebSocket::Handler` et surcharger différentes méthodes qui seront appelées lors de différents évènements :

```c++
struct myHandler : seasocks::WebSocket::Handler {
        
        // Utilisé pour garde en mémoire les clients
        std::set<WebSocket *> connections;
        
        // Appelé lorsqu'un client se conecte
        // *socket est le socket du client
        void onConnect(WebSocket *socket) override{
            connections.insert(socket);
        }
        
        // Appelé lorsqu'un client se connecte
        // *socket est le socket du client
        // *data est la chaine de caractères reçue
        void onData(WebSocket *socket, const char *data) override{
            printf("Requète : %s\n", data);
        }
        
        // Appelé lorsqu'un client se deconnecte
        // *socket est le socket du client
        void onDisconnect(WebSocket *socket) override{
            connections.erase(socket);
        }
    };
```

Il faut ensuite ouvrir la connexion en passant cette structure et en donnant le port :

```c++
// On crée une instance du handler, on le wrap dans un shared_ptr et on garde la ref
auto handler = std::make_shared<myHandler>(myHandler{});
thread t;
auto server = NetworkManager::openConnection(handler, "web", 20999, t);
```

La fonction `openConnection` est non bloquante car elle crée un thread pour ouvrir la connexion.  

**Explication des paramètres :**

- handler ->  `std::shared_ptr<seasocks::WebSocket::Handler>`
- répertoire que le serveur http doit servir -> `char*`
- port sur lequel ouvrir le serveur -> `int`
- _Alors ça mon dieu c'est crade mais j'ai pas trouvé mieux._ On doit donner une variable du type `thread` qui va être affecté par le thread créé dans la fonction. Il faut garder une référence de ce thread sinon l'application s'arrête.

**Renvoi** le serveur du type `seasocks::Server`.

Les serveur HTTP et WebSocket seront ouverts tout les deux .

**Envoyer des données**

Utiliser la fonction `send(string message)` de `seasocks::WebSocket`.  **L'encodage n'est pas fait automatiquement ici !** Cette fonction ne peux que être appelé dans le thread du serveur il faut utiliser la fonction `seasocks::Server::execute(Executable)`. Exemple avec un lambda : 

```c++
// handler est le handler des examples précédents
// data est une chaine de caractères
server->execute([handler, data]{
    for(auto c : handler->connections)
        c->send(data);
});
```

### Autres fonctions

- `char* getAdress(seasocks::WebSocket* socket);`
Retourne l'adresse IPv4 du WebSocket donné sous forme de chaine de charactères.

---

### Encodage

Tout dans la classe `Encodeur` est statique.
[Voir la doc](html/class_encodeur.html)

Petits exemples :
```c++
// Envoyer à tout les clients "supprimer l'objet d'id 12"
for (auto c : connections) c->send(Encodeur::Delete(12));
```

```c++
/* Envoyer à un client "Ajouter une route droite
 *                      à la position 12.5, 45.68
 *                      d'une roation de 28
 *                      d'une longueur de 455
 */
socket->send(Encodeur::AddRoad(12.5, 45.98, 28, 455));
```

### Décodage

Utiliser la fonction `Encodeur::Decode()`  [Voir la doc](html/class_encodeur.html)  
La chaine de caratère retourné est facilement parsable. Elle est de la forme `ACTION_id{données}` ou `ACTION` est égal à `DEL`, `UPD` ou `ADD`.  

---

## Javascript

### Websocket

##### Connexion à un serveur
Il faut inclure dans la page HTML les fichiers `utils.js` puis `Encodeur.js`.
On créé ensuite notre script qui va créer et gérer le websocket.  
_Dans les exemples on utilisera JQuery._
```javascript
// Référence du serveur websocket
var ws;

$(function() {

    // Dans le code C++ le end-point '/city' est utilisé
    ws = new WebSocket('ws://' + document.location.host + '/city');
    
    // Appelé lorsque la connexion est établie avec un serveur
    ws.onopen = function() {
        console.log('onopen');
    };
    
    // Appelé lors de la fermeture de la connexion
    ws.onclose = function() {
        console.log('onclose');
    };

    // Appelé lors de la reception d'un message
    // message est un string
    ws.onmessage = function(message) {
        console.log("got '" + message.data + "'");
    };

    // Appelé lorsque un erreur survient
    ws.onerror = function(error) {
        console.log('onerror ' + error);
    };

});
```

##### Envoyer un message au serveur
```javascript
// message est un string
ws.send(message)
```

---

### Encodage / Décodage

**Énumérations**

```javascript
// Représente l'action à faire
const Action = {
    DELETE : 0b01,
    UPDATE : 0b10
};

// Représente le type d'objet
const CityObject = {
    CAMERA        : 0b000,
    CAR           : 0b011,
    PEDESTRIAN    : 0b100,
    CAR_WITH_PATH : 0b110
};
```

**Encoder une information**

`string encode(objet)`  
Cette fonction retourne un string représentant le message encodé et compressé.
Le paramètre `objet` est une structure de cette forme : 
```javascript
const objet = {
    action   : action,
    type     : cityObjet,
    id       : id,
    variante : variante,
    posx     : posx,
    posy     : posy,
    vitesse  : vitesse,
    zoom     : zoom,

    id_start : id_start,
    id_finish: id_finish
};
```
Les champs inutiles peuvent ne pas exister. Exemples :
```javascript
encode({
    action   : Action.UPDATE,
    type     : CityObject.CAR,
    id       : 12,
    variante : 3,
    posx     : 15.3,
    posy     : 774.36,
    angle    : 222,
    vitesse  : 5
});

encode({
    action   : Action.DELETE,
    id       : 33
});
```

**Décoder une information**

`struct decode(chunk)`  
Cette fonction retourne une structure de la même forme.  
Le paramètre `chunk` est un string.

### Autres fonctions

`string compress(data)`  
Le paramètre `data` **dois être un string représentant un nombre en binaire**.  
Cette fonction retourne une chaine de caractères qui est la version compressé de `data`.

`string decompress(data)`
Cette fonction fais exactement l'inverse de la précédente.

------

### Autres informations

#### Forme des chaînes de caractère parsables

**Delete :** `DEL_{id}`

**Update**:

- Camera : `UPD_0{x,y,zoom}`

- Voiture : `UPD_3{id,variante,x,y,angle,vitesse}`

- Piéton : `UPD_4{id,variante,x,y,angle,vitesse}`

- Voiture avec itinéraire : `UPD_6{id_start,id_finish}`