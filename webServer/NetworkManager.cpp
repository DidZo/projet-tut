#include <arpa/inet.h>
#include "NetworkManager.h"
#include "Encodeur.h"
#include <unistd.h>

#include <utility>

using namespace seasocks;


Server* NetworkManager::openConnection(shared_ptr<WebSocket::Handler> h, char* dir, int port, thread &thread_out) {

    auto* server = new Server(std::make_shared<PrintfLogger>());
    server->addWebSocketHandler("/city", std::move(h));

    thread t = thread([server, dir, port](){
        server->serve(dir, port);
    });

    thread_out = static_cast<thread &&>(t);

    return server;
}

Server* NetworkManager::openConnection(shared_ptr<WebSocket::Handler> h, shared_ptr<WebSocket::Handler> h2, char* dir, int port, thread &thread_out) {

    auto* server = new Server(std::make_shared<PrintfLogger>());
    server->addWebSocketHandler("/city", std::move(h));
    server->addWebSocketHandler("/chat", std::move(h2));

    thread t = thread([server, dir, port](){
        server->serve(dir, port);
    });

    thread_out = static_cast<thread &&>(t);

    return server;
}


char *NetworkManager::getAdress(WebSocket * socket) {
    return inet_ntoa(socket->getRemoteAddress().sin_addr);
}
