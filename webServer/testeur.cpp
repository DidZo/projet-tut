//
// Created by etienne on 12/09/2019.
//

#include <cstdio>
#include <string>
#include <bitset>
#include <iostream>
#include <arpa/inet.h>
#include <unistd.h>
#include <thread>

#include "Encodeur.h"
#include "NetworkManager.h"




int main(int argc, char** argv){

    if(argc < 2)
        argv[1] = "web";

    printf("Lancement du programme de test...\n\n");

    struct myHandler : seasocks::WebSocket::Handler {

        int nbrRequetes = 0;

        std::set<WebSocket *> connections;

        void onConnect(WebSocket *socket) override{
            connections.insert(socket);
            socket->send(Encodeur::UpdateNode(55, 88.3, 99.565));
            socket->send(Encodeur::UpdateCar(55, 0, 88.3, 99.565, 0, 0));
        }

        void onData(WebSocket *socket, const char *data) override{
            nbrRequetes++;
            string decoded = Encodeur::Decode(data);

            // DEBUG
            if(decoded.substr(0, 6) == "debug:")
                for(auto c : connections)
                    c->send(decoded.substr(6, decoded.length()-7));
            // END DEBUG

            printf("Requete numero %i : %s\n", nbrRequetes, decoded.c_str());
        }

        void onDisconnect(WebSocket *socket) override{
            connections.erase(socket);
        }
    };

    struct myChatHandler : seasocks::WebSocket::Handler {

        std::set<WebSocket *> connections;

        void onConnect(WebSocket *socket) override{
            connections.insert(socket);
            socket->send("Bonjour, envoi ton message !");
        }

        void onData(WebSocket *socket, const char *data) override{
            printf("Requete : %s\n", data);
            for(auto c : connections)
                c->send("<span style='color: rgb(111, 0, 255);'>" + (string)NetworkManager::getAdress(socket) + ":</span> " + data);
        }

        void onDisconnect(WebSocket *socket) override{
            connections.erase(socket);
        }

    };

    // = = = Test réseau = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

    thread t;

    auto handler = std::make_shared<myHandler>(myHandler{});
    auto server = NetworkManager::openConnection(handler, argv[1], 20999, t);

    /*while(true){
        //string str = Encodeur::UpdateFurniture(rand()%100,rand()%256,rand()%400-200,rand()%400-200,rand()%360);
        string str = Encodeur::UpdateRoad(rand()%100,rand()%400-200,rand()%400-200,rand()%360,rand()%9900+100, rand()%300+30, false);

        printf("Send %s\n", Encodeur::Decode(str).c_str());

        server->execute([handler, str]{
            for(auto c : handler->connections) {
                c->send(str);
            }
        });

        usleep(1000000);
    }*/

    //printf("%s", Encodeur::Decode("IX??pHo?????k@@??kECOE?").c_str());


    getchar();

    server->terminate();

}